<?php
/* Single carousel-item markup */
?>
<div class="carousel-item <?php if ($args['slide_count'] == 0) {echo "active";} ?>">
	<img class="slider-img w-100" src="<?php the_post_thumbnail_url('slider') ?>">
	<div class="container">
		<div class="carousel-caption text-start">
			<h3 class="mt-3 mb-0"><span class="carousel-heading-span"><?php the_title(); ?></span></h3>
			<?php the_content(); ?>
		</div>
	</div>
</div>
