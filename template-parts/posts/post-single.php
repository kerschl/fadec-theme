<?php
/* Template for displaying a single post */
?>
<article id="post-<?php the_ID(); ?>">
	<div class="post-head-container container-md px-0">
		<h1 class="post-heading"><span class="post-heading-span"><?php the_title(); ?></span></h1>
		<img class="post-head-img" src="<?php echo the_post_thumbnail_url('post-thumb'); ?>" alt="post img">
	</div>
	<div class="d-flex align-items-start">
		<span class="blog-post-meta text-muted me-auto">
			writer: <?php the_author(); ?> date: <?php the_time('j. F Y'); ?>
		</span>
	</div>
	<hr>
	<?php the_content() ?>
	<hr>
</article>
