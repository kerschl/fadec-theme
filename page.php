<?php
/* Template for displaying pages */
get_header();
?>
<article>
	<?php
	echo '<h1>';
	the_title();
	echo '</h1>';
	the_content();
	?>
</article>
<?php get_footer();
