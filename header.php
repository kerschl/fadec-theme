<?php /* The header added to each view */ ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo is_front_page() ? get_bloginfo('name') : wp_title('') ?></title>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/build/styles.css">
	<?php wp_head() ?>
</head>

<body <?php body_class(); ?>>
	<header id="header">
		<nav id="nav" class="navbar navbar-expand-lg">
			<div class="container">
				<a class="navbar-brand" href="<?php bloginfo('wpurl'); ?>">
					<img id="logo" class="logo" alt="fadec" height="60" src="<?php bloginfo('template_directory'); ?>/assets/logo/cropped-fadec-logo-farbe.png">
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<!--?php if (has_nav_menu('main-menu')) {
						wp_nav_menu([
							'theme_location'  => 'main-menu',
							'menu'            => 'main-menu',
							// div
							'container'       => false,
							// ul
							'menu_class'      => 'navbar-nav ms-md-auto',
							'menu_id'         => 'main-menu',
							'walker' => new fadec_main_menu_walker()
							// declared in functions.php
							// https://developer.wordpress.org/reference/functions/wp_nav_menu/
						]);
					} ?-->
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item">
							<a class="nav-link active" aria-current="page" href="/">Articles</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" aria-current="page" href="/aboutus">About Us</a>
						</li>
					</ul>
					<form class="d-flex">
						<input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
						<button class="btn btn-warning" type="submit">Search</button>
					</form>
				</div>
			</div>
		</nav>
	</header>
	<main id="main">
