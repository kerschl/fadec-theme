<?php
// Removes Gutenberg blocks stylesheet
function wpassist_remove_block_library_css()
{
	wp_dequeue_style('wp-block-library');
}
add_action('wp_enqueue_scripts', 'wpassist_remove_block_library_css');

// Removes wp-emoji js
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Force enable sitemap (/wp-sitemap.xml)
add_filter('wp_sitemaps_enabled', '__return_true');
// Sitemap disable users and taxonomies
add_filter('wp_sitemaps_add_provider', 'kama_remove_sitemap_provider', 10, 2);
function kama_remove_sitemap_provider($provider, $name)
{
	$remove_providers = ['users', 'taxonomies'];
	// disabling users archives
	if (in_array($name, $remove_providers)) {
		return false;
	}
	return $provider;
}

// Custom post types
function fadec_post_types()
{
	register_post_type('slide', array(
		'rewrite' => array('slug' => 'slide'),
		'has_archive' => false,
		'public' => false,
		'show_in_rest' => false,
		'show_ui' => true,
		'labels' => array(
			'name' => 'Slides',
			'add_new_item' => 'Neuen Slide erstellen',
			'edit_item' => 'Slide bearbeiten',
			'singular_event' => 'Slide'
		),
		'menu_icon' => 'dashicons-slides',
		'supports' => array( 'title', 'editor', 'thumbnail' )
	));
}
add_action('init', 'fadec_post_types');

// Enable post thumbnails
add_theme_support( 'post-thumbnails' );
add_image_size( 'post-thumb', 2048, 0 );
add_image_size( 'post-item', 600, 300, true );
add_image_size( 'slider', 1920, 800, true );

// add_action( 'after_setup_theme', 'wpdocs_theme_setup' );

// function wpdocs_theme_setup() {
// 		add_image_size( 'post-thumb', 2048, 0 );
// }

